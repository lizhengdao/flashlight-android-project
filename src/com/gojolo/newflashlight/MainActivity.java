package com.gojolo.newflashlight;

import android.app.Activity;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
public class MainActivity extends Activity {
boolean hasflash;
private Camera camera;
Parameters params;
String[] color={
		"#FFFFFF",
		"#dfdfdf",
		"#004488",
		"#880000",
		"#448800",
		"#2175d9",
		"#003366",
		"#0000ff",
		"#0066ff",
		"#00b3e7",
		"#f9b4d6",
		"#003f72",
		"#66ccff",
		"#3d107b",
		"#d600a3",
		"#ff0000",
		"#f1e7a1",
		"#e6aaad",
		"#cbe3e8",
		"#c3eee7",
		"#e8daab",
		"#edc3c5",
		"#ff0066",
		"#637ee9",
		"#3db8c7",
		"#ffdf97",
		"#aaa8aa",
		"#ceaeff",
		"#b7fea0",
		"#40b000",
		"#82f0ff",
		"#7995ba",
		"#d600a3",
		"#ff8f43",
		"#800080",
		"#3db8c7",
		"#29adcd",
		"#ffc1ee",
		"#0f00f0"
};
static int i=1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	    AdView adView = (AdView) this.findViewById(R.id.adView1);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i yüklüyoruz
		final Button on =(Button)findViewById(R.id.on);
		final Button off =(Button)findViewById(R.id.off);
		final ImageView ust = (ImageView)findViewById(R.id.imageView1);
		final RelativeLayout ekran = (RelativeLayout)findViewById(R.id.ekran);
			on.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
				    on.setText("ON");
				    off.setText("OFF");
					ekran.setBackgroundColor(Color.parseColor(color[0]));
					ust.setBackgroundColor(Color.parseColor(color[0]));
					on.setBackgroundResource(Color.TRANSPARENT);
					off.setBackgroundResource(Color.TRANSPARENT);
					light(255);
					
				}

			});
           off.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    on.setText("");
			    off.setText("");
				ekran.setBackground(getResources().getDrawable(R.drawable.ekran));
				ust.setBackground(getResources().getDrawable(R.drawable.a1));
				on.setBackground(getResources().getDrawable(R.drawable.on));
				off.setBackground(getResources().getDrawable(R.drawable.off));
				light(0);
				
			}
		});
           ekran.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(i==38)
					i=0;
				ekran.setBackgroundColor(Color.parseColor(color[i]));
				ust.setBackgroundColor(Color.parseColor(color[i]));
				on.setBackgroundResource(Color.TRANSPARENT);
				off.setBackgroundResource(Color.TRANSPARENT);
				light(255);
			    i++;
				
			}
		});
	    
	}
public void light(int i)
{
    Settings.System.putInt(this.getContentResolver(),
            Settings.System.SCREEN_BRIGHTNESS, i);
}
}
